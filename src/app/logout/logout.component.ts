import { Component, OnInit } from '@angular/core';
import { AuthstateService } from '../authstate.service';
import { Router } from '@angular/router';

@Component({
  template: ''
})
export class LogoutComponent implements OnInit {

  constructor(private auth: AuthstateService,
    private router: Router) { }

  ngOnInit(): void {
    this.auth.logout();
    this.router.navigate(['login'])
  }

}

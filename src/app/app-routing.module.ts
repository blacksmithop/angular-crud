import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactsComponent } from './contacts/contacts.component';
import { ProductsComponent } from './products/products.component';
import { AuthComponent } from './auth/auth.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdditemComponent } from './additem/additem.component';
import { EdititemComponent } from './edititem/edititem.component';
import { NavbarComponent } from './navbar/navbar.component';
import { IsauthGuard } from './isauth.guard'

const routes: Routes = [
  { // Routes that need login (has navbar)
    path: '',
    component: NavbarComponent,
    canActivate: [IsauthGuard],
    children: [
      { path: 'me', component: DashboardComponent },
      { path: 'contacts', component: ContactsComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'logout', component: LogoutComponent },
      { path: 'addproduct', component: AdditemComponent },
      { path: 'editproduct/:id', component: EdititemComponent },
    ]
  },
  { // Public routes
    path: '',
    children: [
      { path: 'login', component: AuthComponent },
      { path: 'register', component: RegisterComponent },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

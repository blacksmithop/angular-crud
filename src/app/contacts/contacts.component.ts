import { Component, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  characters = [
    { id: 1, firstName: `Jeffry`, lastName: 'Houser', email: 'Jeffry@email.com' },
    { id: 2, firstName: `Annie`, lastName: 'Anderson', email: 'Annie@email.com' },
    { id: 3, firstName: `Bob`, lastName: 'Boberson', email: 'Bob@email.com' },
    { id: 4, firstName: `Candy`, lastName: 'Canderson', email: 'Candy@email.com' },
    { id: 5, firstName: `Davey`, lastName: 'Daverson', email: 'Davey@email.com' }
  ]

  constructor(private breakpointObserver: BreakpointObserver,
    private dragulaService: DragulaService) { }

  ngOnInit() {
    this.dragulaService.createGroup('DRAGULA_CONTAINER', {});
  }
}

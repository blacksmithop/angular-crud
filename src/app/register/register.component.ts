import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { AuthstateService } from '../authstate.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../../assets/css/form.css']
})
export class RegisterComponent implements OnInit {

  token: any;
  loginForm = this.form.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  })

  registerForm = this.form.group({
    name: ['', [Validators.required, Validators.minLength(4)]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  })

  constructor(private form: FormBuilder,
    private api: ApiService,
    private router: Router,
    private authService: AuthstateService) {
  }

  ngOnInit(): void {
  }

  register() {
    var name = this.registerForm.value.name
    var email = this.registerForm.value.email
    var password = this.registerForm.value.password
    var data = {
      "name": name,
      "email": email,
      "password": password
    }
    this.api.register(data).subscribe(
      resp => {
        alert("Registered user!");
        //this.router.navigateByUrl("/auth")
      },
      error => {
        console.log(error)

      }
    )
  }


}

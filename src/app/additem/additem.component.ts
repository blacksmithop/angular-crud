import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductmanagerService } from '../productmanager.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-additem',
  templateUrl: './additem.component.html',
  styleUrls: ['./additem.component.css']
})
export class AdditemComponent {
  addItemForm = this.fb.group({
    name: [null, Validators.required],
    type: [null, Validators.required],
    description: [null, Validators.required],
  });

  constructor(private fb: FormBuilder,
    private api: ProductmanagerService,
    private router: Router) { }


  onSubmit() {
    var name = this.addItemForm.value.name
    var type = this.addItemForm.value.type
    var description = this.addItemForm.value.description
    var data = {
      "name": name,
      "description": description,
      "type": type,
    }
    this.api.add(data).subscribe(
      resp => {
        console.log(`Added product ${data}`);
        this.router.navigate(['products']);
      },
      error => {
        console.log(error);
      }
    )
  }
}

import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductmanagerService } from '../productmanager.service'
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edititem',
  templateUrl: './edititem.component.html',
  styleUrls: ['./edititem.component.css']
})
export class EdititemComponent {
  id: Number;
  items: any

  editItemForm = this.fb.group({
    name: [null, Validators.required],
    type: [null, Validators.required],
    description: [null, Validators.required],
  });

  constructor(private fb: FormBuilder,
    private api: ProductmanagerService,
    private router: Router,
    private route: ActivatedRoute) {
    this.id = parseInt(this.route.snapshot.paramMap.get('id')!);
    this.populateList();
  }

  populateList() {
    this.api.list().subscribe((data: any) => {
      var result = data.find(obj => {
        return obj.productId === this.id
      })
      if (result == undefined) {
        alert(`No product with ID ${this.id} exists`)
        this.router.navigate(['products']);
      }
      console.log(result)
      this.editItemForm.get('name')!.setValue(result.name);
      this.editItemForm.get('type')!.setValue(result.type);
      this.editItemForm.get('description')!.setValue(result.description);
    }, error => {
    })
  }

  updateItem() {
    var name = this.editItemForm.value.name
    var type = this.editItemForm.value.type
    var description = this.editItemForm.value.description
    var data = {
      "name": name,
      "description": description,
      "type": type,
    }
    this.api.update(data, this.id).subscribe(
      resp => {
        console.log(`Update product ${data}`);
        this.router.navigate(['products']);

      },
      error => {
        console.log(error);
      }
    )
  }



}

//products-datasource.ts
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ProductsDataSource, ProductsItem } from './products-datasource';
import { ProductmanagerService } from '../productmanager.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<ProductsItem>;
  dataSource: ProductsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'type', 'created', 'updated', 'edit', 'delete'];

  constructor(private api: ProductmanagerService,
    private router: Router,
    public dialog: MatDialog) {
    this.dataSource = new ProductsDataSource(this.api);
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    // for populating table
    this.table.dataSource = this.dataSource;
  }

  addItem() {
    this.router.navigate(['addproduct']);
  }

  deleteDialogue(id) {
    console.log(id)
    var result = this.dataSource.data.find(obj => {
      return obj.productId === id
    })
    if (result == undefined) {
      alert("Does not exist")
    }
    else {
      this.dialog.open(DialogElementsDeleteDialog);
    }
  }


}

@Component({
  selector: 'dialog-elements-delete-dialog',
  templateUrl: './dialogue-delete.html',
})
export class DialogElementsDeleteDialog { }
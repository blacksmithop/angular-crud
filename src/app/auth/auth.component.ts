import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { AuthstateService } from '../authstate.service'

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['../../assets/css/form.css']
})
export class AuthComponent implements OnInit {

  response: any;
  loginForm = this.form.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  })

  registerForm = this.form.group({
    name: ['', [Validators.required, Validators.minLength(4)]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  })

  constructor(private form: FormBuilder,
    private api: ApiService,
    private router: Router,
    private authService: AuthstateService) {
  }

  ngOnInit(): void {
  }
  login() {
    var email = this.loginForm.value.email
    var password = this.loginForm.value.password
    var data = {
      "email": email,
      "password": password
    }
    this.api.login(data).subscribe(
      resp => {
        this.response = resp;
        this.authService.login(this.response.name, this.response.accessToken.value);
      },
      error => {
        console.log(error)

      }
    )
  }
}
